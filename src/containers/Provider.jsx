import React from "react";
import { ThemeProvider } from "styled-components";
import theme from "./theme";

// Setup all providers for the app's react component tree
export default function BBAppProvider({ children }) {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
}
