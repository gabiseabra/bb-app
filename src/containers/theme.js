// Styled components theme
// https://github.com/jxnblk/styled-system
// https://rebassjs.org/theming
export default {
  breakpoints: [
    "300px", // phone
    "600px", // tablet
    "900px" // desktop
  ],
  fontSizes: [12, 14, 16, 24, 32, 48, 64],
  colors: {
    blue: "#6bb2e5",
    pink: "#ea8ca7"
  }
};
