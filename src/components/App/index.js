import React, { Component } from "react";
import { Flex, Box } from "rebass";
import logo from "./logo.svg";
import "./App.css";

function ExampleFlex() {
  return (
    <Flex flexWrap="wrap">
      <Box width={[1, 0.5, 0.25]} p={10} bg="blue" color="lightgray">
        #1
      </Box>
      <Box width={[1, 0.5, 0.25]} p={10} bg="pink" color="white">
        #2
      </Box>
      <Box width={[1, 0.5, 0.25]} p={10} bg="blue" color="lightgray">
        #3
      </Box>
      <Box width={[1, 0.5, 0.25]} p={10} bg="pink" color="white">
        #4
      </Box>
    </Flex>
  );
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <ExampleFlex />
      </div>
    );
  }
}

export default App;
